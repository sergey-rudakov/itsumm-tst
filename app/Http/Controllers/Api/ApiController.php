<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Click;
use App\Models\Page;
use App\Models\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class ApiController extends Controller
{
    protected $host;
    protected $url;
    protected $path;
    protected $user_agent;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recieveClicks( Request $request ) {

        $clicks = $request->json()->get( 'clicks' );

        if ( is_array( $clicks ) && count( $clicks ) > 0 )
        {
            $this->host = $request->header('Origin');
            $this->url = $request->header('Referer');
            $this->path = $this->getPath( $this->host, $this->url );
            $this->user_agent = $request->header('User-Agent');

            if ( $this->host ) {
                try {
                    DB::transaction( function () use( $clicks ) {
                        $resource = $this->getResource();
                        if ( !$resource ) {
                            $resource = new Resource();
                            $resource->host = $this->host;
                            $resource->save();
                        }

                        $page = $this->getPage();
                        if ( !$page ) {
                            $page = new Page();
                            $page->url = $this->url;
                            $page->path = $this->getPath( $this->host, $this->url );
                            $page->user_agent = $this->user_agent;
                            $page->resource()->associate( $resource );
                            $page->save();
                        }

                        $clicksModel = [];
                        foreach( $clicks as $click ) {
                            $clicksModel[] = new Click( $click );
                        }

                        $page->clicks()->saveMany( $clicksModel );

                    }, 5);

                } catch ( Exception $e ) {

                    return response()->json( [
                        'status' => 'error',
                        'errorText' => $e->getMessage()
                    ] );
                } finally {
                    return response()->json( [ 'status' => 'Ok' ] );
                }
            }
        }

        return response()->json( [
            'status' => 'error',
            'errorText' => 'Unknown resource'
        ] );
    }


    /**
     * @param $host
     * @return mixed
     */
    protected function getResource() {
        if ( $this->host ) {
            return Resource::byHost( $this->host )->first();
        }
        return null;
    }

    /**
     * @param $url
     * @return mixed
     */
    protected function getPage() {
        if ( $this->url ) {
            return Page::byUrl( $this->url )->first();
        }
        return null;
    }

    /**
     * @param $url
     * @param $host
     * @return mixed
     */
    protected function getPath( $host, $url ) {
        return str_replace( $host, '', $url  );
    }
}


