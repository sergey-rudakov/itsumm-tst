<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Click;
use App\Models\ClicksByDate;
use App\Models\LastIds;
use App\Models\Page;
use App\Models\Resource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AppController extends Controller
{
    private $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getResources() {
        $resources = Resource::all();

        return response()->json( $resources );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPages() {
        $this->request->validate([ 'id' => 'required' ]);

        $resource = Resource::find( $this->request->id );
        $pages = $resource->pages()->get();

        return response()->json( $pages );
    }

    public function getClicksInDay() {
        $this->request->validate([ 'id' => 'required' ]);

        return response()->json( );
    }

}


