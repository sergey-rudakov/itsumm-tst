<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClicksByArea extends Model
{
    protected $table = 'clicks_by_area';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['xGrid', 'yGrid', 'count', 'page_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function page() {
        return $this->belongsTo( Page::class,'page_id', 'id' );
    }

    /**
     * @param $query
     * @param $obj
     * @return mixed
     */
    public function scopeSame( $query, $obj ) {
        return $query->where( 'xGrid', $obj->xGrid )
            ->where( 'yGrid', $obj->yGrid )
            ->where( 'page_id', $obj->page_id );
    }
}
