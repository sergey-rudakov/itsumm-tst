<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $table = 'resources';

    public function pages() {
        return $this->hasMany( Page::class, 'resource_id', 'id' );
    }

    /**
     * @param $query
     * @param $host
     * @return mixed
     */
    public function scopeByHost( $query, $host ) {
        return $query->where( 'host', $host );
    }
}
