<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LastIds extends Model
{
    protected $table = 'last_ids';

    public function scopeByKey( $query, $key ) {
        return $query->where( 'key', $key );
    }
}
