<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClicksByDate extends Model
{
    protected $table = 'clicks_by_date';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['date', 'hour', 'count', 'page_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function page() {
        return $this->belongsTo( Page::class,'page_id', 'id' );
    }

    /**
     * @param $query
     * @param $obj
     * @return mixed
     */
    public function scopeSame( $query, $obj ) {
        return $query->where( 'date', $obj->date )
                    ->where( 'hour', $obj->hour )
                    ->where( 'page_id', $obj->page_id );
    }
}
