<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';

    public function resource() {
        return $this->belongsTo( Resource::class, 'resource_id', 'id' );
    }

    public function clicks() {
        return $this->hasMany( Click::class, 'page_id', 'id' );
    }

    public function clicksByDate() {
        return $this->hasMany( ClicksByDate::class, 'page_id', 'id' );
    }

    public function scopeByUrl( $query, $url ) {
        return $query->where( 'url', $url );
    }
}
