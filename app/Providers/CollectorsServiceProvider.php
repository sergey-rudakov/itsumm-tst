<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\CollectService;

class CollectorsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap robots services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register robots services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CollectService::class, function ()
        {
            return new CollectService();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [ CollectService::class ];
    }

}
