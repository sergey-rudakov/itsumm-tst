<?php

namespace App\Services;

use App\Models\Click;
use App\Models\ClicksByArea;
use App\Models\ClicksByDate;
use App\Models\LastIds;
use Illuminate\Support\Facades\DB;

class CollectService
{
    private $key_date = 'date_clicks';
    private $key_area = 'area_clicks';
    private $step_grid = 4;

    public function collectByDate() {
        $this->start( $this->key_date, ClicksByDate::class );
    }

    public function collectByArea() {
        $this->start( $this->key_area, ClicksByArea::class );
    }

    public function start( $key, $classname ) {

        $last_counted = LastIds::byKey( $key )->first();

        $start = $last_counted->last_id  + 1;
        $end = Click::orderBy('id', 'desc')->first()->id;

        if ( $end > $start ){

            $get = camel_case ('get_'.$key );
            $count_clicks = $this->$get( $start, $end );

            try {
                DB::transaction( function () use( $count_clicks, $classname ) {
                    foreach ($count_clicks as $count_click) {
                        $same = $classname::same($count_click)->first();

                        if ($same) {
                            $same->count = $same->count + $count_click->count;
                            $same->save();
                        } else {
                            $new = new $classname( (array)$count_click );
                            $new->save();
                        }
                    }
                });
            } finally {
                $last_counted->last_id = $end;
                $last_counted->save();
            }
        }
    }

    /**
     * @param $start
     * @param $end
     * @return array
     */
    private function getDateClicks( $start, $end ) {

        return DB::table('clicks')
            ->select( DB::raw( 'page_id, 
                                            DATE_FORMAT(clicked_at, "%Y-%m-%d") as date,
                                            HOUR(clicked_at) as hour,
                                            count(*) as count' ) )
            ->whereBetween( 'id', [ $start, $end ] )
            ->groupBy( DB::raw( 'page_id, date, hour' ) )
            ->get()
            ->toArray();
    }

    private function getAreaClicks( $start, $end ) {

        return DB::table('clicks')
            ->select( DB::raw( 'page_id, 
                                            ROUND(x / ' . $this->step_grid . ') as xGrid,
                                            ROUND(y / ' . $this->step_grid . ') as yGrid,
                                            count(*) as count' ) )
            ->whereBetween( 'id', [ $start, $end ] )
            ->groupBy( DB::raw( 'page_id, xGrid, yGrid' ) )
            ->get()
            ->toArray();
    }
}
