<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get( '/get-resources', 'AppController@getResources' );
Route::get( '/get-pages', 'AppController@getPages' );
Route::get( '/get-clicks-in-day', 'AppController@getClicksInDay' );

Route::get('/', function () {

    return view('welcome');
});

Route::pattern('path', '[a-zA-Z0-9-/]+');
Route::get('/{path}', function () {

    return view('welcome');
});