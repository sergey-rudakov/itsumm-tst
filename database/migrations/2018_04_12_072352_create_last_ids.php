<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CreateLastIds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('last_ids', function( Blueprint $table ){
            $table->increments('id');
            $table->string('key');
            $table->integer('last_id');
            $table->timestamps();
        });

        $date = Carbon::now('UTC');

        DB::table('last_ids')->insert([
            [
                'id' => 1,
                'key' => 'date_clicks',
                'last_id' => 0,
                'created_at' => $date,
                'updated_at' => $date,
            ],
            [
                'id' => 2,
                'key' => 'area_clicks',
                'last_id' => 0,
                'created_at' => $date,
                'updated_at' => $date,
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'last_ids' );
    }
}
