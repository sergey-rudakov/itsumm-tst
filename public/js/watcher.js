/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/watcher.js":
/***/ (function(module, exports) {

var _createClass = function () {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
        }
    }return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
}();

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

var Watcher = function () {
    function Watcher() {
        _classCallCheck(this, Watcher);

        this.url = 'http://watcher.test:8080/api/send-clicks';
        this.clicks = [];
        this.headers = this.getHeaders();
        this.init();
    }

    _createClass(Watcher, [{
        key: 'clickCounter',
        value: function clickCounter(e) {
            if (e && e.clientX && e.clientY) {
                var date = new Date();
                this.clicks.push({
                    x: e.clientX,
                    y: e.clientY,
                    clicked_at: this.formatDate(date)
                });
            }
        }
    }, {
        key: 'formatDate',
        value: function formatDate(date) {
            var month = date.getUTCMonth() + 1;
            if (month < 10) month = '0' + month;

            return date.getUTCFullYear() + '-' + month + '-' + date.getUTCDate() + ' ' + date.getUTCHours() + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds();
        }
    }, {
        key: 'getHeaders',
        value: function getHeaders() {
            return new Headers({
                "Content-type": "text/plain"
            });
        }
    }, {
        key: 'pushClicks',
        value: function pushClicks() {
            var clicks = this.clicks;
            if (clicks.length > 0) {
                this.clicks = [];

                var data = new FormData();
                data.append('clicks', clicks);

                fetch(this.url, {
                    method: 'POST',
                    //headers: this.headers,
                    mode: 'cors',
                    body: JSON.stringify({ clicks: clicks })
                });
            }
        }
    }, {
        key: 'init',
        value: function init() {
            var self = this;
            document.onclick = function (e) {
                self.clickCounter(e);
            };

            setInterval(function () {
                self.pushClicks();
            }, 3000);
        }
    }]);

    return Watcher;
}();

var watcher = new Watcher();

/***/ }),

/***/ "./resources/assets/sass/app.scss":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("./resources/assets/js/watcher.js");
module.exports = __webpack_require__("./resources/assets/sass/app.scss");


/***/ })

/******/ });