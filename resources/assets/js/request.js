export function request( url, options ) {
    return fetch( url, options ).then(
        response => {
            if ( response.status === 200 ) {
                return response.json();
            } else {
                throw new Error( response.statusText );
            }
        }
    );
}