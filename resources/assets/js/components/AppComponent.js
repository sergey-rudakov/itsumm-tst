import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { ResourcesListComponent } from "./ResorcesListComponent";
import { PageListComponent } from "./PageListComponent";
import { ClickStatComponent } from "./ClickStatComponent";

export class AppComponent extends React.Component {
    constructor( props ) {
        super( props );
    }

    render() {
        return (
            <div>
                <Switch>
                    <Route exact path="/" component={ ResourcesListComponent } />
                    <Route exact path="/resource/:id" component={ PageListComponent } />
                    <Route exact path="/page/:id" component={ ClickStatComponent } />
                </Switch>
            </div>
        );
    }
}