import React from 'react';
import { DayGraphComponent } from "./DayGraphComponent";
import { request } from "../request";

export class ClickStatComponent extends React.Component {
    constructor( props ) {
        super( props );

        this.state = {
            clicks_day_count: [],
        }
    }

    componentDidMount() {
        this.loadClicksInDay();
    }

    loadClicksInDay() {
        const url = '/get-clicks-in-day?id=' + this.props.match.params.id;
        request( url ).then(
            response => {
                console.log( response );
            }
        );
    }

    render() {
        return (
            <div>
                <DayGraphComponent data={ this.state.clicks_day_count } />
            </div>
        );
    }
}