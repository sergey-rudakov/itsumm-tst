import React from 'react';
import { Link } from 'react-router-dom';
import { request } from "../request";

export class PageListComponent extends React.Component {
    constructor( props ) {
        super( props );

        this.state = {
            pages: [],
        }
    }

    componentDidMount() {
        this.loadPages();
    }

    loadPages() {
        const url = '/get-pages?id=' + this.props.match.params.id;
        request( url ).then(
            response => {
                this.setState( { pages: response } );
            }
        );
    }

    renderPages() {
        return this.state.pages.map( res => (
            <div key={ res.id }>
                <Link to={'/page/' + res.id }>{ res.path }</Link>
            </div>
        ) );
    }

    render() {
        return (
            <div>
                <h2>Pages-list</h2>
                {
                    !this.state.pages.length && (
                        <div>Loading...</div>
                    )
                }
                {
                    this.state.pages.length > 0 && (
                        <div>
                            { this.renderPages() }
                        </div>
                    )
                }
            </div>
        );
    }
}