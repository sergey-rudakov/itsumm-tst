import React from 'react';
import { Link } from 'react-router-dom';
import { request } from "../request";

export class ResourcesListComponent extends React.Component {
    constructor( props ) {
        super( props );

        this.state = {
            resources: [],
        }
    }

    componentDidMount() {
        this.loadResources();
    }

    loadResources() {
        request( '/get-resources' ).then(
            response => {
                this.setState( { resources: response } );
            }
        );
    }

    renderResources() {
        return this.state.resources.map( res => (
            <div key={ res.id }>
                <Link to={'/resource/' + res.id }>{ res.host }</Link>
            </div>
        ) );
    }

    render() {
        return (
            <div>
                <h2>Resource-list</h2>
                {
                    !this.state.resources.length && (
                        <div>Loading...</div>
                    )
                }
                {
                    this.state.resources.length > 0 && (
                        <div>
                            { this.renderResources() }
                        </div>
                    )
                }
            </div>
        );
    }
}