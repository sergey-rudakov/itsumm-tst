import React from 'react';
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend
} from 'recharts';

export class DayGraphComponent extends React.Component {
    constructor( props ) {
        super( props );
    }

    render() {
        return (
            <div>
                {
                    this.props.data.length > 0 &&
                    <LineChart width={600} height={300} data={ this.props.data }
                               margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                        <XAxis label="Hours" dataKey="hour" width={24}/>
                        <YAxis label="Clicks"/>
                        <CartesianGrid strokeDasharray="3 3"/>
                        <Tooltip/>
                        <Line type="monotone" dataKey="clicks" stroke="#8884d8" activeDot={{r: 8}}/>
                    </LineChart>
                }
            </div>
        );
    }
}