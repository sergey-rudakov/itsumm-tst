import ReactDOM from 'react-dom';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { AppComponent } from "./components/AppComponent";


window.onload = function() {
    ReactDOM.render(
        <BrowserRouter>
            <AppComponent />
        </BrowserRouter>,
        document.getElementById('root')
    );
}