class Watcher {
    constructor() {
        this.url = 'http://watcher.test:8080/api/send-clicks';
        this.clicks = [];
        this.headers = this.getHeaders();
        this.init();
    }

    clickCounter( e ) {
        if ( e && e.clientX && e.clientY ) {
            const date = new Date();
            this.clicks.push({
                x: e.clientX,
                y: e.clientY,
                clicked_at: this.formatDate( date )
            });
        }
    }

    formatDate( date ) {
        var month = date.getUTCMonth() + 1;
        if ( month < 10 ) month = '0' + month;

        return date.getUTCFullYear() + '-'
                + month + '-'
                + date.getUTCDate() + ' '
                + date.getUTCHours() + ':'
                + date.getUTCMinutes() + ':'
                + date.getUTCSeconds();
     }

    getHeaders() {
        return new Headers( {
            "Content-type": "text/plain"
        } );
    }

    pushClicks() {
        const clicks = this.clicks;
        if ( clicks.length > 0 ) {
            this.clicks = [];

            const data = new FormData();
            data.append( 'clicks', clicks );

            fetch( this.url, {
                method: 'POST',
                //headers: this.headers,
                mode: 'cors',
                body: JSON.stringify({ clicks: clicks })
            } );
        }
    }

    init() {
        const self = this;
        document.onclick = function( e ) {
            self.clickCounter( e );
        };

        setInterval( function() {
            self.pushClicks();
        }, 3000 );
    }
}

const watcher = new Watcher();